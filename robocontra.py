from flask import Flask, url_for, redirect,\
                  request, render_template, flash
import serial
import sys

if len(sys.argv) < 2:
    print "Usage: robocontra.py <serial device name>"
    print "example: sudo python robocontra.py /dev/ttyUSB0"
    exit()

#dev = '/dev/ttyUSB0'
dev = sys.argv[1]
ser = serial.Serial(dev, 9600)

app = Flask(__name__)

DEBUG = True
SECRET_KEY = 'development key'
app.config.from_object(__name__)

@app.route('/', methods=['POST','GET'])
def controller():
    if request.method == 'GET':
        return render_template('controller.html')
    ser.write(request.values['key'])
    #print request.form['key']
    return ''

@app.route('/raw')
def index():
    return render_template('index.html')

@app.route('/send', methods=['POST','GET'])
def send():
    text_to_send = request.form.get('data', '')
    ser.write(text_to_send)
    flash('Text Sent  %s' % text_to_send)
    return redirect(url_for('index'))
    
if __name__ == '__main__':
    app.run(host='0.0.0.0')
